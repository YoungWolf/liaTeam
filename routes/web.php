<?php

use App\Http\Middleware\JsonResponse;
use Laravel\Lumen\Routing\Router;

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('v1/login', 'v1\AuthController@login');
$router->post('v1/logout', 'v1\AuthController@logout');

$router->post('v1/users/', ['middleware' => [JsonResponse::class], 'uses' => 'v1\UserController@create']);

$router->group(['prefix' => 'v1/users', 'middleware' => [JsonResponse::class, 'auth']], function () use ($router) {
    $router->get('/', 'v1\UserController@index');
    $router->get('/{id}', 'v1\UserController@show');
    $router->put('/', 'v1\UserController@update');
    $router->delete('/{id}', 'v1\UserController@delete');

});

/** Role Controller Routes */
$router->group(['prefix' => 'v1/roles', 'middleware' => [JsonResponse::class, 'auth']], function () use ($router) {
    $router->get('/', 'v1\RoleController@index');
    $router->post('/', 'v1\RoleController@create');
    $router->get('/{roleName}', 'v1\RoleController@show');
    $router->delete('/{roleName}', 'v1\RoleController@delete');
    $router->post('/assign-permission', 'v1\RoleController@assignRolePermission');
    $router->post('/assign-user', 'v1\RoleController@assignUserRole');
});

/** Permission Controller Routes */
$router->group(['prefix' => 'v1/permissions', 'middleware' => [JsonResponse::class, 'auth']], function () use ($router) {
    $router->get('/', 'v1\PermissionController@index');
    $router->post('/', 'v1\PermissionController@create');
    $router->get('/{permissionName}', 'v1\PermissionController@show');
    $router->delete('/{permissionName}', 'v1\PermissionController@delete');
    $router->post('/assign', 'v1\PermissionController@assignUserPermission');
});
