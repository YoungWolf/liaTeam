# installation

This sample project implemented using Lumen framework and for run it you can use docker according below:
```
$ docker-compose up -d
$ docker-compose exec app composer install
$ docker-compose exec app php artisan migrate --seed
$ docker-compose exec app php artisan passport:install
```

