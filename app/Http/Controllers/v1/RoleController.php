<?php

namespace App\Http\Controllers\v1;

use App\Builders\RoleBuilder;
use App\Builders\UserBuilder;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class RoleController
 * @package App\Http\Controllers\v1
 */
class RoleController extends Controller
{
    /**
     * @OA\Get(
     *      path="/v1/roles/",
     *      tags={"role"},
     *      summary="roles list",
     *      description="Return list of roles",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="id", type="integer"),
     *                      @OA\Property(property="name", type="string"),
     *                      @OA\Property(property="guard_name", type="string"),
     *                      @OA\Property(property="created_at", type="dateTime"),
     *                      @OA\Property(property="updated_at", type="dateTime"),
     *                  ),
     *              )
     *          )
     *      )
     * )
     */
    public function index(RoleBuilder $roleBuilder): JsonResponse
    {
        return $this->success($roleBuilder->getAllRole(), 200);
    }

    /**
     * @OA\Post(
     *      path="/v1/roles/",
     *      tags={"role"},
     *      summary="create role",
     *      description="Create role according to name, This name must be unique",
     *      @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="name of role",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="data", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *      ),
     * )
     */
    public function create(Request $request, RoleBuilder $roleBuilder): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:roles',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $payload = $request->only('name');
        $roleBuilder->createRole($payload['name']);

        return $this->success('New role has been created successfully', 201);
    }

    /**
     * @OA\Get(
     *      path="/v1/roles/",
     *      tags={"role"},
     *      summary="show role",
     *      description="Return data of specific oermission according to name",
     *      @OA\Parameter(
     *         name="roleName",
     *         in="path",
     *         description="name of role",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="id", type="integer"),
     *                      @OA\Property(property="name", type="string"),
     *                      @OA\Property(property="guard_name", type="string"),
     *                      @OA\Property(property="created_at", type="dateTime"),
     *                      @OA\Property(property="updated_at", type="dateTime"),
     *                  ),
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Bad Request",
     *      ),
     * )
     */
    public function show(string $roleName, RoleBuilder $roleBuilder): JsonResponse
    {
        try {
            return $this->success($roleBuilder->getSpecificRole($roleName), 200);
        } catch (Exception $exception) {
            return $this->error('Role not found', 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/v1/roles/",
     *      tags={"role"},
     *      summary="delete role",
     *      description="Delete specific role according to name",
     *      @OA\Parameter(
     *         name="roleName",
     *         in="path",
     *         description="name of role",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="data", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="internal server error",
     *      ),
     * )
     */
    public function delete($roleName, RoleBuilder $roleBuilder): JsonResponse
    {
        if ($roleBuilder->deleteRole($roleName)) {
            return $this->success('Role has been deleted', 200);
        }

        return $this->error('Can not delete Role!', 500);
    }

    /**
     * @OA\Post(
     *      path="/v1/roles/assign-permission",
     *      tags={"role"},
     *      summary="assign permissions of role",
     *      description="assign permissions of role",
     *      @OA\Parameter(
     *         name="roleName",
     *         in="path",
     *         description="name of role, This name must be exist in db",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Parameter(
     *         name="permissionsName",
     *         in="path",
     *         description="name of permission, This name must be exist in db",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="data", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="internal server error",
     *      ),
     * )
     */
    public function assignRolePermission(Request $request, RoleBuilder $roleBuilder): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'roleName' => 'required|string|exists:roles,name',
            'permissionName' => 'required|string|exists:permissions,name',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $payload = $request->only('roleName', 'permissionName');
        $roleBuilder->assignRolePermission($payload['roleName'], $payload['permissionName']);

        return $this->success('Permission has been assign to role', 200);
    }

    /**
     * @OA\Post(
     *      path="/v1/roles/assign-user",
     *      tags={"role"},
     *      summary="assign role to user",
     *      description="assign role of user, the user and role must be exist in db",
     *      @OA\Parameter(
     *         name="roleName",
     *         in="path",
     *         description="name of role, This name must be exist in db",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Parameter(
     *         name="userId",
     *         in="path",
     *         description="id of user, This name must be exist in db",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="data", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="internal server error",
     *      ),
     * )
     */
    public function assignUserRole(Request $request, RoleBuilder $roleBuilder, UserBuilder $userBuilder): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'roleName' => 'required|string|exists:roles,name',
            'userId' => 'required|string|exists:users,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $payload = $request->only('roleName', 'userId');
        $user = $userBuilder->getSpecificUser('id', $payload['userId']);
        $roleBuilder->assignUserRole($payload['roleName'], $user);

        return $this->success('role has been assign to user', 200);
    }
}
