<?php

namespace App\Http\Controllers\v1;

use App\Builders\PermissionBuilder;
use App\Builders\UserBuilder;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class PermissionController
 * @package App\Http\Controllers\v1
 */
class PermissionController extends Controller
{
    /**
     * @OA\Get(
     *      path="/v1/permissions/",
     *      tags={"permission"},
     *      summary="permission list",
     *      description="Return list of permissions",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="id", type="integer"),
     *                      @OA\Property(property="name", type="string"),
     *                      @OA\Property(property="guard_name", type="string"),
     *                      @OA\Property(property="created_at", type="dateTime"),
     *                      @OA\Property(property="updated_at", type="dateTime"),
     *                  ),
     *              )
     *          )
     *      )
     * )
     */
    public function index(PermissionBuilder $permissionBuilder): JsonResponse
    {
        return $this->success($permissionBuilder->getAllPermission(), 200);
    }

    /**
     * @OA\Post(
     *      path="/v1/permissions/",
     *      tags={"permission"},
     *      summary="create permission",
     *      description="Create permission according to name, This name must be unique",
     *      @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="name of permission",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="data", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *      ),
     * )
     */
    public function create(Request $request, PermissionBuilder $permissionBuilder): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:permissions',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $payload = $request->only('name');
        $permissionBuilder->createPermission($payload['name']);

        return $this->success('New permission has been created successfully', 201);
    }

    /**
     * @OA\Get(
     *      path="/v1/permissions/",
     *      tags={"permission"},
     *      summary="show permission",
     *      description="Return data of specific oermission according to name",
     *      @OA\Parameter(
     *         name="permissionName",
     *         in="path",
     *         description="name of permission",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="id", type="integer"),
     *                      @OA\Property(property="name", type="string"),
     *                      @OA\Property(property="guard_name", type="string"),
     *                      @OA\Property(property="created_at", type="dateTime"),
     *                      @OA\Property(property="updated_at", type="dateTime"),
     *                  ),
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Bad Request",
     *      ),
     * )
     */
    public function show(string $permissionName, PermissionBuilder $permissionBuilder): JsonResponse
    {
        try {
            return $this->success($permissionBuilder->getSpecificPermission($permissionName), 200);
        } catch (Exception $exception) {
            return $this->error('Permission not found', 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/v1/permissions/",
     *      tags={"permission"},
     *      summary="delete permission",
     *      description="Delete specific permission according to name",
     *      @OA\Parameter(
     *         name="permissionName",
     *         in="path",
     *         description="name of permission",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="data", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="internal server error",
     *      ),
     * )
     */
    public function delete($permissionName, PermissionBuilder $permissionBuilder): JsonResponse
    {
        if ($permissionBuilder->deletePermission($permissionName)) {
            return $this->success('Permission has been deleted', 200);
        }

        return $this->error('Can not delete Permission!', 500);
    }

    /**
     * @OA\Post(
     *      path="/v1/permissions/assign",
     *      tags={"permission"},
     *      summary="assign permission",
     *      description="assign specific permission to user",
     *      @OA\Parameter(
     *         name="permissionName",
     *         in="path",
     *         description="name of permission",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Parameter(
     *         name="userid",
     *         in="path",
     *         description="id of user",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="data", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="internal server error",
     *      ),
     * )
     */
    public function assignUserPermission(Request $request, PermissionBuilder $permissionBuilder, UserBuilder $userBuilder): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'permissionName' => 'required|string|exists:permissions,name',
            'userId' => 'required|string|exists:users,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $payload = $request->only('permissionName', 'userId');
        $user = $userBuilder->getSpecificUser('id', $payload['userId']);
        $permissionBuilder->assignUserPermission($payload['permissionName'], $user);

        return $this->success('permission has been assign to user', 200);
    }
}
