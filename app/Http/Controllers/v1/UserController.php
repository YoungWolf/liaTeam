<?php

namespace App\Http\Controllers\v1;

use App\Builders\UserBuilder;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserController
 * @package App\Http\Controllers\v1
 */
class UserController extends Controller
{
    /**
     * @OA\Get(
     *      path="/v1/users/",
     *      tags={"user"},
     *      summary="users list",
     *      description="Return all registered user",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="id", type="integer"),
     *                      @OA\Property(property="name", type="string"),
     *                      @OA\Property(property="email", type="string"),
     *                      @OA\Property(property="email_verified_at", type="string"),
     *                      @OA\Property(property="created_at", type="dateTime"),
     *                      @OA\Property(property="updated_at", type="dateTime"),
     *                  ),
     *              )
     *          )
     *      )
     * )
     */
    public function index(UserBuilder $userBuilder): JsonResponse
    {
        return $this->success($userBuilder->getUsersInfo(), 200);
    }

    /**
     * @OA\Post(
     *      path="/v1/users/",
     *      tags={"user"},
     *      summary="create user",
     *      description="Create user according to name, email and password",
     *      @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="name of user, this name must de unique",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="email",
     *         in="path",
     *         description="email of user, this email must de unique",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="path",
     *         description="password of user",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="data", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *      ),
     *      @OA\Response(
     *          response=500 ,
     *          description="internal server error",
     *      ),
     * )
     */
    public function create(Request $request, UserBuilder $userBuilder): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $payload = $request->only('name', 'email', 'password');
        if ($userBuilder->createUser($payload)) {
            return $this->success('User has been created successfully', 201);
        }

        return $this->error('Can not create user!', 500);
    }

    /**
     * @OA\Get(
     *      path="/v1/users/",
     *      tags={"user"},
     *      summary="show user",
     *      description="Return data of specific oermission according to name",
     *      @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="id of user",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="data",
     *                  type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="id", type="integer"),
     *                      @OA\Property(property="name", type="string"),
     *                      @OA\Property(property="email", type="string"),
     *                      @OA\Property(property="email_verified_at", type="string"),
     *                      @OA\Property(property="created_at", type="dateTime"),
     *                      @OA\Property(property="updated_at", type="dateTime"),
     *                  ),
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Bad Request",
     *      ),
     * )
     */
    public function show(string $id, UserBuilder $userBuilder): JsonResponse
    {
        try {
            return $this->success($userBuilder->getSpecificUser('id', (int)$id), 200);
        } catch (Exception $exception) {
            return $this->error('User not found', 404);
        }
    }

    /**
     * @OA\Update(
     *      path="/v1/users/",
     *      tags={"user"},
     *      summary="create user",
     *      description="Update user according to name, email and password",
     *      @OA\Parameter(
     *         name="name",
     *         in="path",
     *         description="name of user, this name must de unique",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="email",
     *         in="path",
     *         description="email of user, this email must de unique",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="password",
     *         in="path",
     *         description="password of user",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="data", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *      ),
     *      @OA\Response(
     *          response=500 ,
     *          description="internal server error",
     *      ),
     * )
     */
    public function update(Request $request, UserBuilder $userBuilder): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string',
            'email' => 'required|email',
            'password' => 'string'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $payload = $request->only('name', 'email', 'password');
        if ($userBuilder->updateUser($payload)) {
            return $this->success('User has been updated successfully', 200);
        }

        return $this->error('Can not update user!', 500);
    }

    /**
     * @OA\Delete(
     *      path="/v1/users/",
     *      tags={"user"},
     *      summary="delete user",
     *      description="Delete specific user according to id",
     *      @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="id of user",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="data", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=500,
     *          description="internal server error",
     *      ),
     * )
     */
    public function delete($id, UserBuilder $userBuilder): JsonResponse
    {
        if ($userBuilder->deleteUser($id)) {
            return $this->success('User has been deleted', 200);
        }

        return $this->error('Can not delete user!', 500);
    }
}
