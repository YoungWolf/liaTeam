<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Class AuthController
 * @package App\Http\Controllers\v1
 */
class AuthController extends Controller
{
    /**
     * @OA\Post(
     *      path="/v1/login",
     *      tags={"auth"},
     *      summary="For authenticated user",
     *      description="Returns user access token",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="token_type", type="string"),
     *              @OA\Property(property="access_token", type="string"),
     *              @OA\Property(property="expires_in", type="integer"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Bad Request",
     *      ),
     * )
     */
    public function login(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->error($validator->errors(), 401);
        }

        $credentials = $request->only('email', 'password');
        $user = User::where('email', $credentials['email'])->first();
        if ($user instanceof User && Hash::check($request['password'], $user->password)) {
            $token = $user->createToken('Laravel Password Grant Client');
            return $this->success([
                'token_type' => 'Bearer',
                'access_token' => $token->accessToken,
                'expires_in' => Carbon::now()->diffInSeconds($token->token->expires_at),
            ], 200);
        }

        return $this->error('user credentials is wrong', 422);

    }

    /**
     * @OA\Post(
     *      path="/v1/logout",
     *      tags={"auth"},
     *      summary="For authenticated user",
     *      description="When authenticated user wants to logout",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Bad Request",
     *      ),
     * )
     */
    public function logout(): JsonResponse
    {
        $authenticatedUser = Auth::user();
        if (!$authenticatedUser instanceof User) {
            return $this->error('No authenticated user found', 401);
        }
        $token = $authenticatedUser->token();
        $token->revoke();

        return $this->success('You have been successfully logged out!', 200);
    }
}
