<?php

namespace App\Builders;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use RuntimeException;
use Spatie\Permission\Models\Role;

/**
 * Class RoleBuilder
 * @package App\Builders
 */
class RoleBuilder
{
    /**
     * Tries to get all role
     *
     * @return mixed
     */
    public function getAllRole()
    {
        return Role::all();
    }

    /**
     * Tries to create role
     *
     * @param string $roleName
     * @return void
     */
    public function createRole(string $roleName): void
    {
        Role::create(['name' => $roleName]);
    }

    /**
     * Tries to fetch roles
     *
     * @param string $roleName
     * @param bool $returnArray
     * @return array|\Spatie\Permission\Contracts\Role|Role
     */
    public function getSpecificRole(string $roleName, $returnArray = true)
    {
        $role = Role::findByName($roleName);

        if ($returnArray) {
            $role = $role->getAttributes();
            if (!$role) {
                throw new RuntimeException('Role not found');
            }

            return $role;
        }

        return $role;
    }

    /**
     * Tries to delete role
     *
     * @param string $roleName
     * @return bool
     */
    public function deleteRole(string $roleName): bool
    {
        DB::beginTransaction();
        try {
            Role::findByName($roleName)->delete();
            DB::commit();

            return true;
        } catch (Exception $exception) {
            DB::rollBack();

            return false;
        }
    }

    /**
     * Tries to assign permission to role
     *
     * @param string $roleName
     * @param string $permissionName
     * @return void
     */
    public function assignRolePermission(string $roleName, string $permissionName): void
    {
        $role = $this->getSpecificRole($roleName, false);
        $role->givePermissionTo($permissionName);
    }

    /**
     * Tries to assign user to role
     *
     * @param string $roleName
     * @param string $permissionName
     * @return void
     */
    public function assignUserRole(string $roleName, User $user): void
    {
        $role = $this->getSpecificRole($roleName, false);
        $user->assignRole($role);
    }
}
