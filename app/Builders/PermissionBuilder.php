<?php

namespace App\Builders;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use RuntimeException;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionBuilder
 * @package App\Builders
 */
class PermissionBuilder
{
    /**
     * Tries to get all permission
     *
     * @return mixed
     */
    public function getAllPermission()
    {
        return Permission::all();
    }

    /**
     * Tries to create permission
     *
     * @param string $permissionName
     * @return void
     */
    public function createPermission(string $permissionName): void
    {
        Permission::create(['name' => $permissionName]);
    }

    /**
     * Tries to fetch permissions
     *
     * @param string $permissionName
     * @return array
     */
    public function getSpecificPermission(string $permissionName): array
    {
        $permission = Permission::findByName($permissionName)->getAttributes();

        if (!$permission) {
            throw new RuntimeException('Permission not found');
        }

        return $permission;
    }

    /**
     * Tries to delete permissions
     *
     * @param string $permissionName
     * @return bool
     */
    public function deletePermission(string $permissionName): bool
    {
        DB::beginTransaction();
        try {
            Permission::findByName($permissionName)->delete();
            DB::commit();

            return true;
        } catch (Exception $exception) {
            DB::rollBack();

            return false;
        }
    }

    /**
     * Tries to assign permission to user
     *
     * @param string $permissionName
     * @param User $user
     */
    public function assignUserPermission(string $permissionName, User $user): void
    {
        $this->getSpecificPermission($permissionName);
        $user->givePermissionTo($permissionName);
    }
}
