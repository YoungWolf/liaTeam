<?php

namespace App\Providers;

use App\Builders\PermissionBuilder;
use Illuminate\Support\ServiceProvider;

/**
 * Class PermissionServiceProvider
 * @package App\Providers
 */
class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PermissionBuilder::class, function () {
            return new PermissionBuilder();
        });
    }
}
