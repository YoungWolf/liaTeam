<?php

namespace App\Providers;

use App\Builders\RoleBuilder;
use Illuminate\Support\ServiceProvider;

/**
 * Class RoleServiceProvider
 * @package App\Providers
 */
class RoleServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RoleBuilder::class, function () {
            return new RoleBuilder();
        });
    }
}
